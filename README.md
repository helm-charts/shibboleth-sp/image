# Shibboleth SP Image

Contains the images used by the [Shibboleth SP Helm chart](https://gitlab.switch.ch/helm-charts/shibboleth-sp/chart).

Docs: https://int.docs.switch.ch/en/Services/ShibbolethSPHelmChart

## GitLab CI Pipeline:
The pipeline will run separate tasks depending on the type of push:
- Commits:
  - Trigger an image build build with Kaniko but **NOT** push the image to any container registry
- Tags:
  - Trigger an image build build with Kaniko and push the image to this project's container registry
  - Create a release in this project
