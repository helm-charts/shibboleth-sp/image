ARG CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX
FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/ubuntu:jammy

ENV DEBIAN_FRONTEND=noninteractive

# https://packages.cloud.google.com/apt/doc/apt-key.gpg
COPY kubernetes-archive-keyring.gpg /usr/share/keyrings/

RUN apt-get update && \
    apt-get install -y --no-install-recommends apache2 libapache2-mod-shib shibboleth-sp-utils openssl ca-certificates curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    rm -rf /etc/apache2/conf-available && \
    rm -rf /etc/apache2/conf-enabled && \
    rm -rf /etc/apache2/mods-available && \
    rm -rf /etc/apache2/mods-enabled && \
    rm -rf /etc/apache2/sites-available && \
    rm -rf /etc/apache2/sites-enabled && \
    rm -rf /etc/apache2/apache2.conf && \
    rm -rf /etc/apache2/ports.conf && \
    rm -f /etc/shibboleth/attribute-map.xml && \
    rm -f /etc/shibboleth/attribute-policy.xml && \
    rm -f /etc/shibboleth/shibboleth2.xml && \
    rm -f /etc/shibboleth/example-* && \
    rm -f /etc/shibboleth/*.logger && \
    rm -rf /var/log/* && \
    ln -sf /usr/lib/apache2/modules /etc/apache2/modules && \
    ln -sf /dev/shm /etc/apache2/run

RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    install -m 0755 kubectl /usr/local/bin && \
    rm -f kubectl

# https://www.switch.ch/aai/guides/sp/configuration/#setupprofile
# NOTE: attribute-map.xml was manually edited to include all "local attributes" (which are
#       commented out in the donloaded file) and all attributes enabled by "edu-ID only" mode.
COPY attribute-map.xml attribute-policy.xml SWITCHaaiRootCA.crt.pem /etc/shibboleth/
